#include "Aplicatie.h"
#include "UI/MeniuPrincipal.h"

using namespace Calculator::UI;

namespace Calculator {
    Aplicatie::Aplicatie()
    {
    }


    void Aplicatie::Run()
    {
        MeniuPrincipal meniu;

        // bucla interfetei utilizator
        // se executa meniul pana cand utilizatorul apasa exit
        do
        {
            meniu.Execute();

        }while(meniu.Exit() == false);
    }
}
