#ifndef EXCEPTII_H
#define EXCEPTII_H

namespace Calculator {
    namespace Calcule {

        // se declara structura de exceptii
        struct ImpartireLaZero {};
        struct InvalidN {};
        struct InvalidK {};
        struct ParametruInAfaraIntervalului {};
    }
}

#endif // EXCEPTII_H
