#ifndef FIBONACCI_H_INCLUDED
#define FIBONACCI_H_INCLUDED
#include<vector>
#include "siruri.h"
using namespace std;
typedef unsigned int Uint;
typedef vector<Uint> TVint;
typedef vector<Uint>::const_iterator TIterator;
namespace Calculator
{
namespace Calcule
{
class Fibonacci : public Sir
{
public:
    enum {MAX_FIB = 48};
    Fibonacci();
protected:
    void CalculeazaValori(Uint count);

};
}
}

#endif // FIBONACCI_H_INCLUDED
