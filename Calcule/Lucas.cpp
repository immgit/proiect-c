#include "Lucas.h"
#include "Exceptii.h"

typedef unsigned int Uint;

using namespace std;
namespace Calculator
{
namespace Calcule
{
Lucas::Lucas()
{

}
void Lucas::CalculeazaValori(Uint count)
{
    if (count>MAX_LUCAS)
        throw ParametruInAfaraIntervalului();

    if(_elemente.size() == 0)
        _elemente.push_back(2);
    if(_elemente.size() == 1)
        _elemente.push_back(1);

    int size = _elemente.size();
    for(int i=size;i<count;i++)
    {
        _elemente.push_back(_elemente[i-1] + _elemente[i-2]);
    }
}
}
}
