#ifndef LUCAS_H_INCLUDED
#define LUCAS_H_INCLUDED

#include<vector>
#include "siruri.h"
using namespace std;
typedef unsigned int Uint;
typedef vector<Uint> TVint;
typedef vector<Uint>::const_iterator TIterator;
namespace Calculator
{
namespace Calcule
{
class Lucas : public Sir
{
public:
    enum {MAX_LUCAS = 47};
    Lucas();
protected:
    void CalculeazaValori(Uint count);

};
}
}

#endif // LUCAS_H_INCLUDED
