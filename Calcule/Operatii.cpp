#include "Operatii.h"
#include "Exceptii.h"

namespace Calculator {
    namespace Calcule {
        int Adunare(int a, int b)
        {
            return a + b;
        }


        int Scadere(int a, int b)
        {
            return a - b;
        }


        int Inmultire(int a, int b)
        {
            return a * b;
        }


        int Impartire(int a, int b)
        {
            if(0 == b)
            {
                throw ImpartireLaZero();
            }

            return a / b;
        }

    }
}
