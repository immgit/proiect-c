#include "Pell.h"
#include "Exceptii.h"

typedef unsigned int Uint;

using namespace std;
namespace Calculator
{
namespace Calcule
{
Pell::Pell()
{

}
void Pell::CalculeazaValori(Uint count)
{
    if (count>MAX_PELL)
        throw ParametruInAfaraIntervalului();

    if(_elemente.size() == 0)
    {
        _elemente.push_back(0);
        _elemente.push_back(1);
    }
    Uint size = _elemente.size();

    for(int i=size;i<count;i++)
    {
        _elemente.push_back(2 * _elemente[i-1] + _elemente[i-2]);
    }
}
}
}
