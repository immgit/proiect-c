#ifndef PELL_H_INCLUDED
#define PELL_H_INCLUDED

#include<vector>
#include "siruri.h"
using namespace std;
typedef unsigned int Uint;
typedef vector<Uint> TVint;
typedef vector<Uint>::const_iterator TIterator;
namespace Calculator
{
namespace Calcule
{
class Pell : public Sir
{
public:
    enum {MAX_PELL = 27};
    Pell();
protected:
    void CalculeazaValori(Uint count);

};
}
}

#endif // PELL_H_INCLUDED
