#include "Pentagonal.h"
#include "Exceptii.h"

typedef unsigned int Uint;

using namespace std;
namespace Calculator
{
namespace Calcule
{
Pentagonal::Pentagonal()
{

}
void Pentagonal::CalculeazaValori(Uint count)
{
    if (count>MAX_PENTAGONAL)
        throw ParametruInAfaraIntervalului();

    _elemente.push_back(0);

    int i=0;
    Uint size = _elemente.size();

    while(size<count)
    {
        i++;
        double rez = (i * (3 * i - 1))/2;
        _elemente.push_back((Uint) rez);


        size = _elemente.size();
    }



}
}
}
