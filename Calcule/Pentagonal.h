#ifndef PENTAGONAL_H_INCLUDED
#define PENTAGONAL_H_INCLUDED

#include<vector>
#include "siruri.h"
using namespace std;
typedef unsigned int Uint;
typedef vector<Uint> TVint;
typedef vector<Uint>::const_iterator TIterator;
namespace Calculator
{
namespace Calcule
{
class Pentagonal : public Sir
{
public:
    enum {MAX_PENTAGONAL = 1024};
    Pentagonal();
protected:
    void CalculeazaValori(Uint count);

};
}
}

#endif // PENTAGONAL_H_INCLUDED
