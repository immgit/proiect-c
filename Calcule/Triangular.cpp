#include "Triangular.h"
#include "Exceptii.h"

typedef unsigned int Uint;

using namespace std;
namespace Calculator
{
namespace Calcule
{
Triangular::Triangular()
{

}
void Triangular::CalculeazaValori(Uint count)
{
    if (count>MAX_TRIANGULAR)
        throw ParametruInAfaraIntervalului();


     _elemente.push_back(0);

    int i=0;
    Uint size = _elemente.size();

    while(size<count)
    {
        i++;
        double rez = (i * (i + 1))/2;
        _elemente.push_back((Uint) rez);


        size = _elemente.size();
    }
}
}
}
