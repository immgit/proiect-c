#ifndef TRIANGULAR_H_INCLUDED
#define TRIANGULAR_H_INCLUDED

#include<vector>
#include "siruri.h"
using namespace std;
typedef unsigned int Uint;
typedef vector<Uint> TVint;
typedef vector<Uint>::const_iterator TIterator;
namespace Calculator
{
namespace Calcule
{
class Triangular : public Sir
{
public:
    enum {MAX_TRIANGULAR = 1024};
    Triangular();
protected:
    void CalculeazaValori(Uint count);

};
}
}

#endif // TRIANGULAR_H_INCLUDED
