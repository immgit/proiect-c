#include "combinatorial.h"
#include "Exceptii.h"

namespace Calculator
{
namespace Calcule
{
int Factorial(int n)
{
    int factorial=1;
    if(n<0 || n>12)
        throw InvalidN();

    if(n==1)
        return 1;

    for(int i=n; i>0; i--)
    {
        factorial*=i;
    }
    return factorial;
}

int Aranjamente(int n, int k)
{
    if(k<0 || k>n)
        throw InvalidK();

    return (Factorial(n))/(Factorial(n-k));

}

int Combinari(int n, int k)
{
    return Aranjamente(n,k)/Factorial(k);
}


}
}
