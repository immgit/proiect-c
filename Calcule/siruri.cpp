#include "Exceptii.h"
#include "siruri.h"
#include <vector>
#include <iomanip>

using namespace std;

namespace Calculator
{
    namespace Calcule
    {
        Sir::Sir() : _count(0)
        {

        }
        Sir::~Sir()
        {
        }

        Uint Sir::operator [] (Uint index) const
        {
            if(index<0 || index >= _elemente.size())
                throw ParametruInAfaraIntervalului();
            return _elemente[index];
        }

        Sir& Sir::operator() (Uint count)
        {
            _count = count;

            this->CalculeazaValori(_count);

            return *this;
        }

        ostream& operator << (ostream &out, const Sir &sir)
        {
            out<<"Index     Valoare"<<endl;
            for(int i=0;i<sir._elemente.size();i++)
            {
            out<<setw(4)<<right<<i;
            out<<setw(12)<<right<<sir._elemente[i]<<endl;
            }

            return out;
        }
    }
}
