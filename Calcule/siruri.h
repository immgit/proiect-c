#ifndef SIRURI_H_INCLUDED
#define SIRURI_H_INCLUDED

#include <vector>
#include <fstream>
using namespace std;
typedef unsigned int Uint;
typedef vector<Uint> TVint;
typedef vector<Uint>::const_iterator TIterator;


namespace Calculator
{
    namespace Calcule
    {
class Sir{
    protected:
        Uint _count;
        TVint _elemente;
        virtual void CalculeazaValori(Uint count)=0;
    public:
        Sir();
        virtual ~Sir();
        Uint operator [] (Uint index) const;
        Sir& operator () (Uint count);
        friend ostream& operator << (ostream& out, const Sir &sir);



};
}}

#endif // SIRURI_H_INCLUDED
