//*****************************************************************************
// Fisierul AfisazaMeniu.h
// Contine declaratia clasei AfiseazaMeniu folosita pentru afisarea numelui comenzilor
//*****************************************************************************
#ifndef AFISEAZAMENIU_H
#define AFISEAZAMENIU_H

#include <iostream>
#include <iomanip>
#include "Comanda.h"
using namespace std;

namespace Calculator {
	namespace UI {
        //==============================================================
        // AfisazaMeniu este un obiect functie care afisaza
        // indexul si numele comenzilor unui meniu
		class AfiseazaMeniu
		{
			int _index; // indexul comenzii curente

		public:
			AfiseazaMeniu() : _index(0)
			{
			}

            //==============================================================
            // operator functie apelat din algoritmul STL for_each
            // parametru este comanda curenta
            // scrie pe ecran indexul si numele comenzii
			void operator() (Comanda *comanda)
			{
				cout << setw(3) << right << _index++ << " " << comanda->Nume() << "\n";
			}
		};

	}
}
#endif
