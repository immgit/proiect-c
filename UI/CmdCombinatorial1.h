#ifndef CMDCOMBINATORIAL1_H_INCLUDED
#define CMDCOMBINATORIAL1_H_INCLUDED

#include <iostream>
#include "Comanda.h"

// se include headerul pentru functiile ajutatoare
#include "../util.h"

// se includ headerele din calcule
#include "../Calcule/combinatorial.h"
#include "../Calcule/Exceptii.h"


using namespace std;

using namespace Calculator::Calcule;

namespace Calculator{
namespace UI{
    template <int Operatie(int), int MAX, int MIN = 0>
    class CmdCombinatorial1 : public Comanda
    {
    public:
        CmdCombinatorial1(const string &nume) : Comanda(nume)
        {
        }

        void Execute()
        {
            cout<<"Introduceti un numar intreg in intervalul ["<<MIN<<", "<<MAX<<")\n";

            int x;
            cout<<"x: ";
            cin>>x;

            try{
                cout << x << " " << Nume() <<" = " << Operatie(x) << "\n";
            }catch(InvalidN&){
                cout<<"Numarul dat nu este corect\n";
            }

            ::AsteaptaUser();
        }
    };


    template <int Operatie(int,int), int MAX, int MIN = 0>
    class CmdCombinatorial2 : public Comanda
    {
    public:
        CmdCombinatorial2(const string &nume) : Comanda(nume)
        {
        }

        void Execute()
        {
            cout<<"Introduceti doua numere intregi (n, k, n > k) in intervalul ["<<MIN<<", "<<MAX<<"]\n";

            int n,k;
            cout<<"n: ";
            cin>>n;
            cout<<"k: ";
            cin>>k;

            try{
                cout << Nume() <<" ("<<n<< ", "<< k << ") = " << Operatie(n,k) << "\n";
            }catch(InvalidN&){
                cout<<"n invalid\n";
            }
            catch(InvalidK&)
            {
                cout<<"k invalid\n";
            }

            ::AsteaptaUser();
        }
    };

}
}





#endif // CMDCOMBINATORIAL1_H_INCLUDED
