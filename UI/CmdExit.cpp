#include "CmdExit.h"

namespace Calculator {
    namespace UI {

        CmdExit::CmdExit(const string &nume) : Comanda(nume), _exit(false) // foloseste lista de initializare
        {
        }

        void CmdExit::Execute()
        {
            _exit = true;
        }


        bool CmdExit::Exit() const
        {
            return _exit;
        }
    }
}
