//*****************************************************************************
// Fisierul CmdExit.h
// Contine declararea clasei de baza Comanda care executa comenzi utilizator
//
//*****************************************************************************

#ifndef CMDEXIT_H
#define CMDEXIT_H


#include "Comanda.h"

namespace Calculator {
    namespace UI {

        //==============================================================
        // Clasa specializata pentru terminarea programului sau
        // submeniurilor
        class CmdExit : public Comanda
        {
        public:
            CmdExit(const string &nume = "Exit");

            //==============================================================
            // Suprascrie metoda execute care seteaza atributul _exit
            virtual void Execute();

            //==============================================================
            // Returneaza true daca a fost apelata metoda Execute
            // sau false altfel.
            bool Exit() const;


        private:
            bool _exit;
        };
    }
}
#endif // CMDEXIT_H
