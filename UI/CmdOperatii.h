#ifndef CMDOPERATII_H
#define CMDOPERATII_H

#include <iostream>
#include "Comanda.h"

// se include headerul pentru functiile ajutatoare
#include "../util.h"

// se includ headerele din calcule
#include "../Calcule/Operatii.h"
#include "../Calcule/Exceptii.h"


using namespace std;

// se adauga directiva using pentru calcule
using namespace Calculator::Calcule;

namespace Calculator {
    namespace UI {

        // se implementeaza clasa comanda
        // pentru simplitate metodele sunt implementate inline.
        template<int Functie(int a, int b)>
        class CmdOperatii : public Comanda
        {
        public:
            // se defineste constructorul
            CmdOperatii(const string &nume) : Comanda(nume)
            {
            }


            // se suprascrie metoda Execute
            void Execute()
            {
                // se citesc valorile de la utilizator:
                cout << "Introduceti doua numere intregi\n";

                int x, y;
                cout << "x: ";
                cin >> x;

                cout << "y: ";
                cin >> y;

                // se executa comanda
                try
                {
                    // metoda Nume() este declarata in clasa Comanda.
                    // functie vine ca parametru in lista de parametri generici
                    cout << x << " " << Nume() <<  " " << y << " = " << Functie(x, y) << "\n";
                }
                // se trateaza exceptiile
                catch(ImpartireLaZero &)
                {
                    cout << "Impartire la zero\n";
                }

                // se asteapta ca utilizatorul sa citeasca rezultatul
                // functia este definita in spatiul de nume global
                ::AsteaptaUser();

            }
        };
    }
}

#endif // CMDOPERATII_H
