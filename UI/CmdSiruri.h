#ifndef CMDSIRURI_H_INCLUDED
#define CMDSIRURI_H_INCLUDED
#include <iostream>
#include "Comanda.h"

// se include headerul pentru functiile ajutatoare
#include "../util.h"

// se includ headerele din calcule
#include "../Calcule/Fibonacci.h"
#include "../Calcule/Exceptii.h"


using namespace std;

using namespace Calculator::Calcule;

namespace Calculator{
namespace UI{
    template <typename SirNumere, int MAX, int MIN = 0>
    class CmdSiruri : public Comanda
    {
    private:
        SirNumere _sir;
    public:
        CmdSiruri(const string &nume) : Comanda(nume)
        {
        }

        void Execute()
        {
            cout<<"Introduceti un numar intreg in intervalul ["<<MIN<<", "<<MAX<<")\n";

            int n;
            cout<<"n: ";
            cin>>n;

            try{
                cout << "Sirul "<< Nume() << "[0 - "<< n-1<<"]:\n"<<_sir(n)<<endl;

            }catch(ParametruInAfaraIntervalului&){
                cout<<"Parametru in afara intervalului\n";
            }

            ::AsteaptaUser();
        }
    };
    }
    }


#endif // CMDSIRURI_H_INCLUDED
