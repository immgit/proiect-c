#include <iostream>
#include "Comanda.h"

using namespace std;

namespace Calculator{
    namespace UI {
        Comanda::Comanda(const string &nume) : _nume (nume) // foloseste lista de initializare
        {
        }


        Comanda::~Comanda()
        {
        }


        const string& Comanda::Nume() const
        {
            return _nume;
        }
    }
}
