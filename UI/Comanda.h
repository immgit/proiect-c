//*****************************************************************************
// Fisierul Comanda.h
// Contine declararea clasei de baza Comanda care executa comenzi utilizator
//
//*****************************************************************************
#ifndef COMANDA_H
#define COMANDA_H

#include <string>

using std::string;

// spatiul de nume pentru aplicatie
namespace Calculator {

    // spatiul de nume pentru modulul de interfata utilizator
    namespace UI{

        // Clasa de baza care executa o comanda pentru utilizator
        class Comanda
        {
            const string _nume; // numele comenzii executate


        public:
            Comanda(const string &nume);
            virtual ~Comanda();


            const string& Nume() const;  // returneaza numele comenzii

            //==============================================================
            // Metoda abstracta pentru executia comenzilor
            // In clasele derivate trebuie sa:
            // - solicita datele de intrare de la utilizator
            // - citeste datele de intrare in variabile locale
            // - executa calculele necesare
            // - afisaza rezultatele calculelor
            // - trateaza exceptiile ridicate de efectuarea calculelor
            // - afisaza mesajele de eroare corespunzatoare exceptiilor
            // - apeleaza metoda AsteaptaEnter - pentru a permite
            //   utilizatorului citirea rezultatelor inainte de
            //   stergerea ecranului
            virtual void Execute() = 0;
        };

    }
}
#endif // COMANDA_H
