//*****************************************************************************
// Fisier: IndexComandaInvalid.h
// Contine declararea clasei IndexComandaInvalid
// Exceptie ridicata atunci cand indexul comenzii introdus este invalid.
//*****************************************************************************
#ifndef INDEXCOMANDAINVALID_H
#define INDEXCOMANDAINVALID_H

namespace Calculator {
    namespace UI {

        // exemplu de clasa ridicata ca exceptie din program
        struct IndexComandaInvalid {};
    }
}


#endif // INDEXCOMANDAINVALID_H
