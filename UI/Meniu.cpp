#include <algorithm>
#include <iostream>
#include "../util.h"
#include "AfiseazaMeniu.h"
#include "IndexComandaInvalid.h"
#include "CmdExit.h"
#include "Meniu.h"


namespace Calculator{
    namespace UI {
        Meniu::Meniu(const string &nume, const string &exitName) : Comanda(nume)
        {
            AdaugaComanda(new CmdExit(exitName)); // Se adauga comanda de exit.
        }

        // Functie care elibereaza memoria pentru o comanda.
        void DeleteComanda(Comanda *&comanda)
        {
            delete comanda;
            comanda = nullptr;
        }


        Meniu::~Meniu()
        {
            // se elibereaza memoria ocupata de comenzi cu algoritmul for_each si functia DeleteComanda
            for_each(_comenzi.begin(), _comenzi.end(), DeleteComanda);
        }


        void Meniu::Execute()
        {
            Index indexComanda = 0;

            while(true) // se ruleaza bucla pana cand utilizatorul introduce un index valid
            {
                AfisazaComenzi();

                try
                {
                    indexComanda = CitesteIndexComanda();
                    break;
                }
                catch(IndexComandaInvalid &) // se foloseste transfer prin referinta
                {
                    continue;
                }
            }

            StergeEcran();
            _comenzi[indexComanda]->Execute(); // Executa comanda selectata

        }


        void Meniu::AfisazaComenzi()
        {
            StergeEcran();
            cout << Nume() << ":\n";
            for_each(_comenzi.begin(), _comenzi.end(), AfiseazaMeniu()); // Foloseste obiectul functie AfisazaMeniu pentru scrierea numelor comenzilor.
        }



		Meniu::Index Meniu::CitesteIndexComanda()
		{
            ReseteazaErori(cin);

            cout << "\nIntroduceti indexul comenzii: ";

            Meniu::Index index = 0;
			cin >> index;

            ValideazaIndexComanda(index);			

			return index;
		}



		void Meniu::ValideazaIndexComanda(Index indexComanda)
		{
		    if(cin.fail() || indexComanda < 0  || indexComanda >= _comenzi.size())
            {
                throw IndexComandaInvalid();
            }
		}


		void Meniu::AdaugaComanda(Comanda *comanda)
		{
		    _comenzi.push_back(comanda);
		}



		bool Meniu::Exit()
		{
		    // Se face dynamic cast la clasa derivata CmdExit pentru a putea apela metoda Exit
		    // In acest caz este util dynamic_cast pentru ca verifica tipul instantei
		    // daca prima comanda nu este de tipul CmdExit atunci programul ridica o exceptie
		    // si eroarea este usor de depistat
            return dynamic_cast<CmdExit*>(_comenzi.front())->Exit();
		}

    }
}
