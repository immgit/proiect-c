//*****************************************************************************
// Fisierul Meniu.h
// Contine declaratia clasei Meniu derivata din clasa Comanda
//*****************************************************************************

#ifndef MENIU_H
#define MENIU_H

#include <vector>
#include "Comanda.h"

using namespace std;

namespace Calculator {
    namespace UI {

        //==============================================================
        // Clasa specializata care contine o lista de comenzi
        class Meniu : public Comanda
        {
            // declaratii de aliasuri de tip pentru simplificarea codului
            typedef vector<Comanda*> TCommands;
			typedef TCommands::size_type Index;

			TCommands _comenzi; // colectia de comenzi

		public:

		    //==============================================================
		    // Constructor
		    // - la instantiere adauga automat o comanda de exit pe prima pozitie
		    // - parametrul exitName specifica numele comenzii de exit adaugate
			Meniu(const string &nume, const string &exitName = "Exit");

			virtual ~Meniu();

            //==============================================================
            // Metoda Execute
            // - afisaza pe ecran numele comenzilor
            // - solicita introducerea indexului comenzii ce trebuie executate
            // - citeste indexul comenzii
            // - daca indexul este valid executa comanda cu indexul respectiv
			virtual void Execute();


			//==============================================================
			// Metoda Exit
			// - testeaza daca prima comanda (care este comanda Exit) a fost executata
			bool Exit();

        protected:
            //==============================================================
            //Metoda pentru adaugarea comenzilor din clasele derivate
			void AdaugaComanda(Comanda *comanda);

		private:


            //==============================================================
            // Afisaza numele comenzilor
		    void AfisazaComenzi();

			//==============================================================
			// Citeste indexul comenzii
			// - afisaza mesaj de informare
			// - citeste indexul
			// - valideaza indexul - ridica exceptia IndexComandaInvalid
			// Nota: tipul de return este Meniu::Index deoarece aliasul pentru Index este definit
			// in interiorul clasei Meniu
			Meniu::Index CitesteIndexComanda();

			//==============================================================
			// Valideaza indexul comenzii
			// - ridica exceptia IndexComandaInvalid
			void ValideazaIndexComanda(Index indexComanda);

        };

    }
}

#endif // MENIU_H
