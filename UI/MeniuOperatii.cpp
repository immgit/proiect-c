// se adauga directivele de include necesare
#include "MeniuOperatii.h"
#include "CmdOperatii.h"
#include "../Calcule/Operatii.h"

// se adauga directiva de using pentru calcule
using namespace Calculator::Calcule;

namespace Calculator {
    namespace UI {

        // Se apeleaza constructorul clasei de baza cu numele meniului
        MeniuOperatii::MeniuOperatii() : Meniu("Demo: Operatii Aritmetice")
        {
            // Se adauga comenzile meniului:
            // - in lista de parametri generici se transfera functia sau clasa pentru calcule
            // - in parametri se adauga numele comenzii
            AdaugaComanda(new CmdOperatii<Adunare>("+"));
            AdaugaComanda(new CmdOperatii<Scadere>("-"));
            AdaugaComanda(new CmdOperatii<Inmultire>("*"));
            AdaugaComanda(new CmdOperatii<Impartire>("/"));

        }
    }
}
