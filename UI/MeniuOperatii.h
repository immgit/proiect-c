#ifndef MENIUOPERATII_H
#define MENIUOPERATII_H

#include "Meniu.h"

namespace Calculator {
    namespace UI {

        // se declara clasa si constructorul implicit
        class MeniuOperatii : public Meniu
        {
        public:
            // constructorul se implementeaza in fisierul cpp
            // pentru reducerea diretivelor include in hader
            MeniuOperatii();
        };
    }
}

#endif // MENIUOPERATII_H
