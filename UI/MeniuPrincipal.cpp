#include "MeniuPrincipal.h"

// se adauga include pentru submeniuri
#include "MeniuOperatii.h"
#include "meniucombinatorial.h"
#include "MeniuSiruri.h"


namespace Calculator {
    namespace UI {
        MeniuPrincipal::MeniuPrincipal() : Meniu("Proiect C++")
        {
            AdaugaComanda(new MeniuOperatii());
            AdaugaComanda(new MeniuCombinatorial());
            AdaugaComanda(new MeniuSiruri());


        }
    }
}
