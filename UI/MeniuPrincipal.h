#ifndef MENIUPRINCIPAL_H
#define MENIUPRINCIPAL_H

#include "Meniu.h"

namespace Calculator {
    namespace UI {
        class MeniuPrincipal : public Meniu
        {
        public:
            MeniuPrincipal();
        };
    }
}

#endif // MENIUPRINCIPAL_H
