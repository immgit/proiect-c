#include "MeniuSiruri.h"
#include "CmdSiruri.h"
#include "../Calcule/Fibonacci.h"
#include "../Calcule/Pell.h"
#include "../Calcule/Triangular.h"
#include "../Calcule/Lucas.h"
#include "../Calcule/Pentagonal.h"

// se adauga directiva de using pentru calcule
using namespace Calculator::Calcule;

namespace Calculator {
    namespace UI {

        // Se apeleaza constructorul clasei de baza cu numele meniului
        MeniuSiruri::MeniuSiruri() : Meniu("Siruri")
        {
            // Se adauga comenzile meniului:
            // - in lista de parametri generici se transfera functia sau clasa pentru calcule
            // - in parametri se adauga numele comenzii
            AdaugaComanda(new CmdSiruri<Fibonacci,Fibonacci::MAX_FIB,0 >("Fibonacci"));
            AdaugaComanda(new CmdSiruri<Lucas,Lucas::MAX_LUCAS,0>("Lucas"));
            AdaugaComanda(new CmdSiruri<Pell,Pell::MAX_PELL,0>("Pell"));
            AdaugaComanda(new CmdSiruri<Triangular,Triangular::MAX_TRIANGULAR,0>("Triangular"));
            AdaugaComanda(new CmdSiruri<Pentagonal,Pentagonal::MAX_PENTAGONAL,0>("Pentagonal"));
        }
    }
}
