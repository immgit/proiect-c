#ifndef MENIUSIRURI_H_INCLUDED
#define MENIUSIRURI_H_INCLUDED

#include "Meniu.h"

namespace Calculator {
    namespace UI {

        // se declara clasa si constructorul implicit
        class MeniuSiruri : public Meniu
        {
        public:
            // constructorul se implementeaza in fisierul cpp
            // pentru reducerea diretivelor include in hader
            MeniuSiruri();
        };
    }
}

#endif // MENIUSIRURI_H_INCLUDED
