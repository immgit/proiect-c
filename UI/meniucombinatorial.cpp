#include "meniucombinatorial.h"
#include "CmdCombinatorial1.h"
#include "../Calcule/combinatorial.h"

// se adauga directiva de using pentru calcule
using namespace Calculator::Calcule;

namespace Calculator {
    namespace UI {

        // Se apeleaza constructorul clasei de baza cu numele meniului
        MeniuCombinatorial::MeniuCombinatorial() : Meniu("Operatii Combinatorice")
        {
            // Se adauga comenzile meniului:
            // - in lista de parametri generici se transfera functia sau clasa pentru calcule
            // - in parametri se adauga numele comenzii
            AdaugaComanda(new CmdCombinatorial1<Factorial,MAX,0>("!"));
            AdaugaComanda(new CmdCombinatorial2<Aranjamente,MAX,0>("Aranjamente"));
            AdaugaComanda(new CmdCombinatorial2<Combinari,MAX,0>("Combinari"));

        }
    }
}
