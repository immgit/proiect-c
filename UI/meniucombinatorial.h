#ifndef MENIUCOMBINATORIAL_H_INCLUDED
#define MENIUCOMBINATORIAL_H_INCLUDED

#include "Meniu.h"

namespace Calculator {
    namespace UI {

        // se declara clasa si constructorul implicit
        class MeniuCombinatorial : public Meniu
        {
        public:
            // constructorul se implementeaza in fisierul cpp
            // pentru reducerea diretivelor include in hader
            MeniuCombinatorial();
        };
    }
}

#endif // MENIUCOMBINATORIAL_H_INCLUDED
