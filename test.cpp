#include "test.h"

// Se adauga headerele pentru testarea fiecarei functionalitati in program
#include <iostream>
#include "util.h"
#include "Calcule/Exceptii.h"
#include "Calcule/Operatii.h"
#include "Calcule/combinatorial.h"
#include "Calcule/Fibonacci.h"
#include "Calcule/siruri.h"



#include "UI/CmdExit.h"
#include "UI/Meniu.h"
#include "UI/CmdOperatii.h"
#include "UI/MeniuOperatii.h"
#include "UI/MeniuPrincipal.h"

#include "Aplicatie.h"

//TODO: se adauga directivele include necesare





// regiune de include pentru testare
// poate contine orice directiva de using
using namespace Calculator;
using namespace Calculator::Calcule;
using namespace Calculator::UI;



void TestOperatii()
{
    int x = 10;
    int y = 4;

    int suma = Adunare(x, y);
    int diferenta = Scadere(x, y);
    int produs = Inmultire(x, y);
    int impartire = Impartire(x, y);


}


void TestExceptiiOperatii()
{
    // testarea erorilor
    try
    {
        Impartire(5, 0);
    }
    catch(ImpartireLaZero &)
    {
        cout << "Impartire la zero\n";
    }
}


void TestCmdOperatii()
{
    CmdOperatii<Adunare> op("+");
    op.Execute();
}


void TestMeniuOperatii()
{
    MeniuOperatii meniu;
    meniu.Execute();
}


void TestAplicatie()
{
    Aplicatie app;
    app.Run();
}

void TestCombinatorial()
{
    int x = 5;
    int fact;
    try{
        fact = Factorial(x);
        cout<<fact<<endl;
    }
    catch(InvalidN &)
    {
        cout<<"Nu se poate calcula factorial de "<<x;
    }


    int n = 4;
    cout<<"\nAranjamente\n";
    try{
    for(int i=1;i<=n;i++)
    {
        cout<<"Aranajamente de "<<n<<" luate cate "<<i<<": "<<Aranjamente(n,i)<<endl;
        cout<<"Combinari de "<<n<<" luate cate "<<i<<": "<<Combinari(n,i)<<endl;
    }
    }
    catch(InvalidN&)
    {
        cout<<"Exceptie de la factorial";
    }
    catch(InvalidK&)
    {
    cout<<"Exceptie de la k";
    }



}

void TestFibonacci()
{
    try{
        Fibonacci f;
        cout<<f(5);
        cout<<f[4];
    }
    catch(ParametruInAfaraIntervalului&)
    {
        cout<<"Parametru invalid";
    }


}


// TODO: se implementeaza functii de test pentru fiecare functionalitate adaugata



void Test()
{
    //TestOperatii();

    // TestExceptiiOperatii();

    //TestCmdOperatii();

    //TestMeniuOperatii();

    TestAplicatie();

    //TestCombinatorial();

    //TestFibonacci();

    //TODO: se atauga apelurile de test si se comenteaza cele anterioare
}
