#include <iostream>
#include <cstdlib>

using namespace std;

void AsteaptaUser()
{
    cout << "Apasati o tasta pentru a continua ";
    cin.ignore(); // se ignora newline
	cin.get();    // se citeste un caracter
}


// Functie care sterge ecranul
void StergeEcran()
{
#if defined(_WIN32) || defined(_WIN64)
	#define ERASE "cls"
#else
	#define ERASE "clear"
#endif
	system(ERASE);
}
