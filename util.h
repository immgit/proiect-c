//*****************************************************************************
// Fisierul Util.h
// contine macro instructiuni si functii template care simplifica scrierea codului
//*****************************************************************************
#ifndef UTIL_H
#define UTIL_H

#include <limits>

//==============================================================
// defineste nullptre daca versiunea de C++ este mai mica decat C++11
// se foloseste: http://www.stroustrup.com/C++11FAQ.html#11
#if (__cplusplus == 199711L) || (__cplusplus < 199711L)
    #define nullptr 0
#endif // __cplusplus



//==============================================================
// Functie care ignora linii noi din buffer
// este utila la citiri cand se apeleaza atat functiile get, getline
// cat si operatorul >>
template<typename Stream>
void IgnoraNL(Stream &stream)
{
	stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//==============================================================
// Functie template care reseteaza erorile de citire
template<typename Stream>
void ReseteazaErori(Stream &stream)
{
    if(stream.fail())
    {
        stream.clear();
		IgnoraNL(stream);        
    }
}


//==============================================================
// Functie care asteapta input de la utilizator
void AsteaptaUser();


//==============================================================
// Functie care sterge ecranul
void StergeEcran();

#endif /* UTIL_H */

